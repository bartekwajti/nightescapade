import Vue from "vue";
import VueRouter from "vue-router";
import Particles from "../views/Particles";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Particles",
    component: Particles
  },
  {
    path: "/plan-route",
    name: "RoutePlanner",
    component: () => import("../views/PlanRoute.vue")
  }
];

const router = new VueRouter({
  routes
});

export default router;

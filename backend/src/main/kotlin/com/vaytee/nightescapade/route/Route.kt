package com.vaytee.nightescapade.route

import com.vaytee.nightescapade.location.Location
import javax.persistence.*

@Entity
data class Route(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,

        val name: String,

        @OneToMany(mappedBy = "route", cascade = [CascadeType.ALL])
        val locations: List<Location>)
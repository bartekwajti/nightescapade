package com.vaytee.nightescapade.route

import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/route")
class RouteController(val repository: RouteRepository) {

    @PostMapping
    fun addRoute(@RequestBody route: Route): Route = repository.save(route)

    @GetMapping
    fun getRoute(id: Long): Route = repository.getById(id)
}
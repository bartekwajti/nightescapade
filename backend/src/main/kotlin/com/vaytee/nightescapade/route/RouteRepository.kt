package com.vaytee.nightescapade.route

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface RouteRepository : JpaRepository<Route, Long> {
    fun getById(id : Long) : Route
}
package com.vaytee.nightescapade.config

import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@EnableJpaRepositories
class JpaRepositoriesConfig
package com.vaytee.nightescapade

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class NightescapadeApplication

fun main(args: Array<String>) {
    runApplication<NightescapadeApplication>(*args)
}

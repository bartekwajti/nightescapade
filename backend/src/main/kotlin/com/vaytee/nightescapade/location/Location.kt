package com.vaytee.nightescapade.location

import com.vaytee.nightescapade.route.Route
import javax.persistence.*

@Entity
data class Location(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long? = null,

        val longitude: Double,
        val latitude: Double,

        @ManyToOne(fetch = FetchType.LAZY)
        val route: Route? = null)
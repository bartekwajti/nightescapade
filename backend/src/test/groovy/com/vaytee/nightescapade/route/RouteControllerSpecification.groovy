package com.vaytee.nightescapade.route


import io.zonky.test.db.AutoConfigureEmbeddedDatabase
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.test.context.ActiveProfiles
import spock.lang.Specification
import spock.lang.Unroll

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles('disabled-flyway')
@AutoConfigureEmbeddedDatabase
class RouteControllerSpecification extends Specification {

    @Autowired
    private TestRestTemplate restTemplate


    @Unroll
    def 'when POST /route is invoked then response contains id and name "#name"'() {
        def request = [
                'name'     : name,
                'locations': [
                        ['latitude': 44.21, 'longitude': 21.44],
                        ['latitude': 11.11, 'longitude': 22.22]
                ]
        ]

        when:
        def responseEntity = restTemplate.postForEntity('/route', request, Route.class)

        then:
        def resultRoute = responseEntity.body
        resultRoute.id != null
        resultRoute.name == expectedName

        where:
        name            | expectedName
        'route #1'      | 'route #1'
        'another route' | 'another route'

    }

}
